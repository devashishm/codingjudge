package com.codejudge.addTwoNos.bean;

public class Sum {
	
	int sum;
	
	public Sum(int sum) {
		super();
		this.sum = sum;
	}

	/**
	 * @return the sum
	 */
	public int getSum() {
		return sum;
	}

	/**
	 * @param sum the sum to set
	 */
	public void setSum(int sum) {
		this.sum = sum;
	}

}
