package com.codejudge.addTwoNos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodejudgePlatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodejudgePlatformApplication.class, args);
	}

}
