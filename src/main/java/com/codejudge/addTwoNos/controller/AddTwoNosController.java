package com.codejudge.addTwoNos.controller;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.codejudge.addTwoNos.bean.Numbers;
import com.codejudge.addTwoNos.bean.Sum;

@SpringBootApplication
@RestController
public class AddTwoNosController {

	@PostMapping(value = "/api/add", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Sum> fetchData(@RequestBody Numbers numbers) {

		int number1 = numbers.getNumber1();
		int number2 = numbers.getNumber2();
		int sum = number1 + number2;
		
		Sum responseBody = new Sum(sum);
		return new ResponseEntity<Sum>(responseBody, HttpStatus.OK);
	}

}
